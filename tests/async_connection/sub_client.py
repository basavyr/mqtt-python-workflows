from paho.mqtt import client as mqtt
import paho.mqtt.subscribe as subscribe
import time as time
from timeit import default_timer


class MQTT_Callbacks:
    """ 
    - functions that are used to override the default callbacks of the client, when performing certain actions 
    """
    @ staticmethod
    def on_connect(client, userdata, flags, rc):
        if rc != 0:
            print(f'Issues while connecting to broker -> {rc}')
        else:
            print(f'Connection returned rc -> {rc}')

    @ staticmethod
    def on_disconnect(client, userdata, rc):
        if rc != 0:
            print(f'Unexpected disconnection')
        else:
            print(f'Disconnected from the server with rc -> {rc}')

    @ staticmethod
    def on_subscribe(client, userdata, mid, granted_qos):
        print(f'Subscribed to broker with mid -> {mid}')

    @staticmethod
    def on_message(client, userdata, message):
        encoded_message = message.payload
        topic = message.topic
        decoded_message = encoded_message.decode('utf-8')
        print(f'MSG: {decoded_message} on TOPIC: {str(topic)}')


class MQTT_Subscriber:
    ClientName = 'AsyncSubscriber'
    Broker = 'broker.hivemq.com'
    PORT = 1883
    TestTopic = f'Bishu-2'

    def __init__(self, subscribe_time):
        self.topic = MQTT_Subscriber.TestTopic
        self.broker = MQTT_Subscriber.Broker
        self.clientName = MQTT_Subscriber.ClientName
        self.port = MQTT_Subscriber.PORT
        self.subscribeTime = subscribe_time

    def createClient(self):
        tempClient = mqtt.Client(client_id=self.clientName)

        tempClient.on_connect = MQTT_Callbacks.on_connect
        tempClient.on_disconnect = MQTT_Callbacks.on_disconnect
        tempClient.on_subscribe = MQTT_Callbacks.on_subscribe
        tempClient.on_message = MQTT_Callbacks.on_message

        return tempClient

    def AsyncSubscribe(self):
        start_time = 0

        client = self.createClient()

        client.connect_async(host=self.broker, port=self.port)
        time.sleep(1)

        client.loop_start()
        time.sleep(1)

        client.subscribe(topic=self.topic)
        time.sleep(1)
        while start_time <= self.subscribeTime:
            start_time = start_time + 1
            time.sleep(1)

        client.loop_stop()
        time.sleep(1)

        client.disconnect()


def main():
    subClient = MQTT_Subscriber(10)
    subClient.AsyncSubscribe()


if __name__ == '__main__':
    main()
