from paho.mqtt import client as mqtt
import paho.mqtt.publish as publish
import time as time
from timeit import default_timer

from random import randrange, uniform
import numpy as np


def topic_generator(N_topics):
    """
    - generates a list of strings, representing the TOPICS on which the client will subscribe
    - size of the topic list is given by the N_topics argument
    """
    topics = []

    for topic_id in range(N_topics):
        current_topic = f'Bishu-{topic_id+1}'
        topics.append(current_topic)

    return list(map(str, topics))


def data_generator(n_data, seed):
    """
    - generate a random array of size n_data
    - seed represents the absolute value of the element with smallest/largest value
    """
    data = []
    for idx in range(n_data):
        rd_array = [np.round(uniform(-seed, seed), 3) for _ in range(3)]
        # every element within the array must be string
        data.append(str(rd_array))
    return data


def multi_messages(topics, data):
    """
    - create a multi-message list for the mqtt publisher
    - the elements of the list are given as tuples (topic,data)
    - both the topic and the data must be strings
    """
    if(len(topics) != len(data)):
        return -1
    else:
        msgs = [x for x in zip(topics, data)]
        return msgs


class MQTT_Callbacks:
    """ 
    - functions that are used to override the default callbacks of the client, when performing certain actions 
    """
    @ staticmethod
    def on_connect(client, userdata, flags, rc):
        if rc != 0:
            print(f'Issues while connecting to broker -> {rc}')
        else:
            print(f'Connection returned rc -> {rc}')

    @ staticmethod
    def on_disconnect(client, userdata, rc):
        if rc != 0:
            print(f'Unexpected disconnection')
        else:
            print(f'Disconnected from the server with rc -> {rc}')

    @ staticmethod
    def on_publish(client, userdata, mid):
        print(f'Data as been published on the broker with mid -> {mid}')


class MQTT_Publisher:
    ClientName = 'AsyncPublisher'
    Broker = 'broker.hivemq.com'
    PORT = 1883

    def __init__(self, data):
        self.data = data
        self.topics = topic_generator(len(data))
        self.clientName = MQTT_Publisher.ClientName
        self.broker = MQTT_Publisher.Broker
        self.port = MQTT_Publisher.PORT

    def showInfo(self):
        print(f'Created the client <<< {self.clientName} >>>')
        print(f'Created the topics <<< {self.topics} >>>')
        print(f'Client will connect to <<< {self.broker} >>>')

    def createClient(self, debug):
        """
        - implement a client that will connect to a broker
        - client is defined using the standard mqtt constructor
        - the client is initialized with all the necessary callbacks
        """
        # create the client with custom name
        if(debug == 1):
            print(f'<<< In the async connection >>>')
            print(f'...Creating the client using MQTT constructor...')
        tempClient = mqtt.Client(client_id=self.clientName)

        if(debug == 1):
            self.showInfo()

        # set the client callbacks
        if(debug == 1):
            print(f'...Setting up the callbacks...')
        tempClient.on_connect = MQTT_Callbacks.on_connect
        tempClient.on_disconnect = MQTT_Callbacks.on_disconnect
        tempClient.on_publish = MQTT_Callbacks.on_publish

        if(debug == 1):
            print(f'...Finish setting up the client...')
        return tempClient

    def connectAsync(self, connection_time):
        """
        - create a asynchronous connection with the broker
        - the connection will persist for a fixed amount of time
        - client will publish data to the server that is given within the `data` argument
        """
        start_time = 0

        client = self.createClient(debug=0)
        time.sleep(1)

        # establish asynchronous connection with the host
        client.connect_async(host=self.broker, port=self.port)
        time.sleep(1)

        # changing the internal callback for the publisher does not work
        # publish._on_publish = MQTT_Callbacks.on_publish

        # activate the loop between the client and the broker
        # ***************************************************
        client.loop_start()
        time.sleep(1)
        # ***************************************************
        while start_time <= connection_time:

            # perform a publish to the broker on a single topic
            # client.publish(topic=self.topics[0], payload=str(data))

            # perform the publish for multiple topics
            # METHOD 1
            # for current_topic in self.topics:
            #     client.publish(topic=current_topic, payload=str(data))

            # perform the publish for multiple topics
            # METHOD 2
            multi_msgs = multi_messages(self.topics, self.data)
            publish.multiple(
                msgs=multi_msgs, hostname=self.broker)

            start_time = start_time + 1
            time.sleep(1)
        # ***************************************************
        client.loop_stop()
        time.sleep(1)
        # ***************************************************

        client.disconnect()


def main():
    # generate the data
    n_data = 3
    seed = 1
    data = data_generator(n_data, seed)

    # publish the data
    pubClient = MQTT_Publisher(data)
    pubClient.connectAsync(connection_time=10)


if __name__ == "__main__":
    main()
