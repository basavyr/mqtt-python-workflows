import paho.mqtt.client as mqtt

import time

MQTT_Broker = "mqtt.eclipseprojects.io"


# implement a callback function that will be executed when the CONNACK response is received from the server
def on_connect(client, userdata, flags, rc):
    # https://github.com/eclipse/paho.mqtt.python#more-information
    print(f'On_Connect callback ->{str(rc)}')

    client.subscribe(f"{MQTT_Broker}/#")


# callback function that is executed when a PUBLISH message is received from the server
def on_message(client, userdata, rc):
    print(f'server response code ->{str(rc)}')


# create the client
client = mqtt.Client("CLIENT")
client.on_connect = on_connect
client.on_message = on_message


MQTT_PORT = 1883
TIMEOUT = 60
# establish the connections
client.connect(MQTT_Broker, MQTT_PORT, TIMEOUT)
client.loop_forever()
