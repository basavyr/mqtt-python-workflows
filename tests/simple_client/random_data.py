import numpy as np
from random import randrange, uniform, randint
from matplotlib import pyplot as plt

TYPE_UNI = 'uniform'
TYPE_rand = 'rand'


def giveUniformNumber(seed):
    """
    return a random number using uniform
    """
    retval = uniform(-seed, seed)
    return retval


def giveRandNumber(seed):
    """
    return a random number using randrange
    """
    retval = randrange(-seed, seed)
    return retval


def generateArray(size, seed, type):
    ret_arr = []
    for idx in range(size):
        if(type == 'rand'):
            ret_arr.append(giveRandNumber(seed))
        elif type == 'uniform':
            ret_arr.append(giveUniformNumber(seed))
    return ret_arr


if __name__ == '__main__':
    main()
