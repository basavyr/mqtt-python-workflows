import time


def wait(wait_time):
    """
    - create a timer that blocks the current thread
    - time is given in seconds
    """
    time.sleep(wait_time)


def log_on_connect(client, userdata, flags, rc):
    """The callback for when the client receives a CONNACK response from the server."""
    print(f'Connected with result code {str(rc)} 🤝')
    # should implement a subscribe
    # Subscribing in on_connect() means that if we lose the connection and reconnect then subscriptions will be renewed.
    # source https://github.com/eclipse/paho.mqtt.python#usage-and-api


def log_on_disconnect(client, userdata, rc):
    """The callback for when the client disconnects from the server"""
    print(f'Connection ended and returned result {str(rc)} ❌')


def log_on_message(client, userdata, msg):
    """The callback for when a PUBLISH message is received from the server"""
    print(f'MESSAGE: {msg.topic} + {str(msg.payload)}')


def log_on_subscribe(client, userdata, mid, granted_qos):
    """The callback for when he broker/server responds to a subscribe request"""
    print(
        f'🔂 A subscription has been established with the server/broker"')


def log_on_publish(client, userdata, mid):
    """the callback for then data is PUBLISHED to a server"""
    print(f'Data has been published to broker with mid={str(mid)} ✎')
