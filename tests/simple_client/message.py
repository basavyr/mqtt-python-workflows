import random_data


def pullMessage():
    """
    - define a random array starting from an arbitrary array size
    - type of random data must also be specified (i.e., uniform or randrange types)
    - the seed (integer value representing the absolute value of the smallest and largest values within the generated data)
    """

    arr_type = 'uniform'
    # arr_size = random_data.randint(10, 100)
    arr_size = 3
    arr_seed = 9

    arr = [random_data.np.round(x, 2) for x in random_data.generateArray(
        arr_size, arr_seed, arr_type)]

    # print(f'Size of array -> {arr_size}\nArray -> {arr}')

    return {"message": arr, "size": len(arr)}
