import paho.mqtt.client as mqtt

import publisher as pub
import message as mess
import topic
import callbacks

from timeit import default_timer
# use the default timer to measure execution time of any call within the codebase
# source https://stackoverflow.com/a/25823885/8295213


class MQTT_ClientSubscribe:
    # source for the hive broker
    # https://www.hivemq.com/public-mqtt-broker/
    Hive_Broker = 'broker.hivemq.com'
    Eclipse_Broker = 'mqtt.eclipseprojects.io'

    # use a predefined topic
    Topic = topic.Topic()
    # use a predefined client name
    ClientName = 'Subscriber'

    def __init__(self, subscriptionTime):
        self.subscriptionTime = subscriptionTime
        self.topic = MQTT_ClientSubscribe.Topic
        self.clientName = MQTT_ClientSubscribe.ClientName
        self.broker = MQTT_ClientSubscribe.Hive_Broker

    def Info(self):
        print(f'Client Name -> {self.clientName}')
        print(f'Topic -> {self.topic}')
        print(f'Connection time -> {self.connectionTime}')

    def setCallbacks(self, client):
        try:
            client.on_connect = callbacks.log_on_connect
            client.on_publish = callbacks.log_on_publish
            client.on_message = callbacks.log_on_message
            client.on_disconnect = callbacks.log_on_disconnect
            client.on_subscribe = callbacks.log_on_subscribe
        except Exception as issue:
            print(
                f'There was an issue while setting the callback for the client methods...\n<<< {issue} >>>')
        else:
            pass

    def createClient(self):
        """
        - creates a client that can publish data to an mqtt broker
        - the client name is already set within the class-init process
        - uses the callbacks defined in the callback module
        """

        # create the mqtt client via the Client
        temp_client = mqtt.Client(self.clientName)

        # set the callbacks of the created client
        self.setCallbacks(temp_client)

        return temp_client

    def SubUnsub(self):
        """
        - create a client that subscribes to a server
        - after one server-client loop perform an unsubscribe from the broker
        - subscribe again the next cycle
        - runtime is given by the subscription time 
        """
        client = self.createClient()

        start_time = 0

        print(f'Attempt connection to the {self.broker}...')
        client.connect(self.broker)
        client.loop()

        while(start_time <= self.subscriptionTime):
            (sub_result, sub_mid) = client.subscribe(self.topic)
            callbacks.wait(1)
            client.loop()
            print(f'Sub-{start_time} -> {sub_result} , {sub_mid}')
            (unsub_result, unsub_mid) = client.unsubscribe(self.topic)
            callbacks.wait(1)
            client.loop()
            print(f'Unsub-{start_time} -> {unsub_result} , {unsub_mid}')
            start_time = start_time + 1

        print(f'Perform a disconnect from {self.broker}...')
        client.disconnect()

    def establishSubscription(self):
        subscriber = self.createClient()

        start_time = 0

        print(f'Attempt connection to the {self.broker}...')
        subscriber.connect(self.broker)
        subscriber.loop_start()

        subscriber.subscribe(self.topic)

        while(start_time <= self.subscriptionTime):
            callbacks.wait(1)
            start_time = start_time + 1

        subscriber.loop_stop()
        subscriber.disconnect()


def main():
    mainClient = MQTT_ClientSubscribe(subscriptionTime=100)
    mainClient.establishSubscription()


if __name__ == '__main__':
    main()
