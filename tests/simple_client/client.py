import paho.mqtt.client as mqtt
import random_data

import callbacks as cll


def pullMessage():
    """
    - define a random array starting from an arbitrary array size
    - type of random data must also be specified (i.e., uniform or randrange types)
    - the seed (integer value representing the absolute value of the smallest and largest values within the generated data)
    """

    arr_type = 'uniform'
    # arr_size = random_data.randint(10, 100)
    arr_size = 3
    arr_seed = 9

    arr = [random_data.np.round(x, 2) for x in random_data.generateArray(
        arr_size, arr_seed, arr_type)]

    # print(f'Size of array -> {arr_size}\nArray -> {arr}')

    return {"message": arr, "size": len(arr)}


class MQTT_ClientPublish:
    # source for the hive broker
    # https://www.hivemq.com/public-mqtt-broker/
    Hive_Broker = 'broker.hivemq.com'
    Eclipse_Broker = 'mqtt.eclipseprojects.io'

    def __init__(self, clientName, connectionTime):
        self.clientName = clientName
        self.connTime = connectionTime
        self.broker = MQTT_ClientPublish.Hive_Broker

    def createClient(self):
        temp_client = mqtt.Client(self.clientName)
        temp_client.on_connect = cll.log_on_connect
        temp_client.on_disconnect = cll.log_on_disconnect
        # temp_client.on_message = cll.log_on_message
        temp_client.on_publish = cll.log_on_publish
        return temp_client

    def customLoop(self, topic, loopTime):
        client = self.createClient()

        print(f'Will attempt connection to {self.broker}')

        start_time = 0
        client.connect(self.broker)  # server does not respond until loop stars
        cll.wait(1)
        client.loop_start()
        while start_time < loopTime:
            client.publish(topic, 1)
            cll.wait(1)
            start_time = start_time + 1
        client.loop_stop()
        client.disconnect()


def MQTT_Client_Publish(run_time):
    """
    - Creating a client that will publish data on the broker
    - The connection -loop- has a FIXED duration, given by the loop's start and stop procedures, manually placed within the codebase
    - repeats = number of publishing iterations
    """

    Eclipse_Broker = 'mqtt.eclipseprojects.io'
    Hive_Broker = 'broker.hivemq.com'

    # create the client with a fixed ID
    client_name = 'Randomizer'
    client = mqtt.Client(client_name)
    client.on_connect = cll.log_on_connect
    # client.on_message = cll.log_on_message
    # no need for declaring the `on_message` callback since the publisher does not subscribe to the topic
    client.on_publish = cll.log_on_publish

    local_topic = 'arrays'

    # establish connection to the broker
    print(f'Establishing connection...')
    client.connect(Hive_Broker)

    # start the loop (handshake with the server so the CONNACK completes)
    # add a pause before pulling and publishing the message, so the CONNACK will finish
    client.loop_start()
    print(f'Loop started...')
    cll.wait(1)

    startime = 0
    while startime < run_time:
        # generate a message
        print(f'Preparing the message to be published...')
        full_msg = pullMessage()
        print(f'Will publish -> {full_msg["message"]} | {full_msg["size"]}')
        # publish the message after it was generated
        client.publish(local_topic, str(full_msg["message"]))
        startime = startime + 1
        cll.wait(1)

    # # keep the loop running until the runtime as finished
    # cll.wait(run_time)
    # stop the loop after the message has been published
    client.loop_stop()
    print(f'Loop ended')


def main():
    # MQTT_Client_Publish(5)
    mainClient = MQTT_ClientPublish('Randomizer', 10)
    mainClient.customLoop('arrays', 5)


if __name__ == '__main__':
    main()
