import paho.mqtt.client as mqtt

import callbacks as cll
import publisher as pub
import message as mess
import topic as topic

from timeit import default_timer
# use the default timer to measure execution time of any call within the codebase
# source https://stackoverflow.com/a/25823885/8295213


class MQTT_ClientPublish:
    # source for the hive broker
    # https://www.hivemq.com/public-mqtt-broker/
    Hive_Broker = 'broker.hivemq.com'
    Eclipse_Broker = 'mqtt.eclipseprojects.io'

    # use a predefined client name
    ClientName = 'Randomizer'
    # use a predefined topic
    Topic = topic.Topic()

    def __init__(self):
        self.clientName = MQTT_ClientPublish.ClientName
        self.topic = MQTT_ClientPublish.Topic
        self.broker = MQTT_ClientPublish.Hive_Broker

    def setCallbacks(self, client):
        client.on_connect = cll.log_on_connect
        client.on_publish = cll.log_on_publish
        client.on_message = cll.log_on_message
        client.on_disconnect = cll.log_on_disconnect

    def createClient(self):
        """
        - creates a client that can publish data to an mqtt broker
        - the client name is already set within the class-init process
        - uses the callbacks defined in the callback module
        """

        # create the mqtt client via the Client
        temp_client = mqtt.Client(self.clientName)

        # set the callbacks of the created client
        self.setCallbacks(temp_client)

        return temp_client

    def customLoop(self, loopTime):
        client = self.createClient()

        start_time = 0

        # establish connection to the server and wait for the on_connect callback
        print(f'Will attempt connection to {self.broker}')
        client.connect(self.broker)  # server does not respond until loop stars

        # wait one second so that the connection callback is executed before the loop starts
        cll.wait(1)

        # ******************************* #
        client.loop_start()
        while start_time < loopTime:
            # implement message to be published on the server
            client.publish(self.topic, 1)
            cll.wait(1)
            start_time = start_time + 1
        # stop the loop after all the data has been published to the broker
        client.loop_stop()
        # ******************************* #

        # disconnect from the server/broker
        client.disconnect()

    def continuousLoop(self, loopTime):
        client = self.createClient()

        start_time = 0

        # attempt connection to the predefined broker
        print(f'Will attempt connection to {self.broker}')
        client.connect(self.broker)
        cll.wait(1)

        # implement message to be published on the server
        client.publish(self.topic, 1)

        # ******************************* #
        # client.loop_forever()  # source: https://github.com/eclipse/paho.mqtt.python#usage-and-api
        # ******************************* #
        # the forever loop should be placed at the end of the script
        while(start_time <= loopTime):
            start_time = start_time + 1
            cll.wait(1)

        # disconnect from the server/broker
        client.disconnect()

    def soloLoop(self, loopTime):
        client = self.createClient()

        start_time = 0

        client.connect(self.broker)
        # client.loop()  # using a single loop call so that the network sends the CONNACK response back to the client

        while(start_time <= loopTime):
            start_time = start_time + 1

            start = default_timer()
            client.loop()
            end = default_timer()
            # print(end - start)
            # implement message to be published on the server
            client.publish(self.topic, 1)

            cll.wait(1)

    def publishMessages(self, loopTime):
        start_time = 0

        publisher = self.createClient()

        publisher.connect(self.broker)
        publisher.loop_start()

        while(start_time <= loopTime):
            message = [1]
            publisher.publish(self.topic, str(message))
            start_time = start_time + 1
            cll.wait(1)

        publisher.loop_stop()
        publisher.disconnect()


def main():
    # pub.MQTT_Client_Publish(5)
    mainClient = MQTT_ClientPublish()
    mainClient.publishMessages(100)


if __name__ == '__main__':
    main()
