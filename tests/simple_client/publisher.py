import paho.mqtt.client as mqtt

import callbacks as cll
import message as mess
import topic as topic


def MQTT_Client_Publish(run_time):
    """
    - Creating a client that will publish data on the broker
    - The connection -loop- has a FIXED duration, given by the loop's start and stop procedures, manually placed within the codebase
    - repeats = number of publishing iterations
    """

    Eclipse_Broker = 'mqtt.eclipseprojects.io'
    Hive_Broker = 'broker.hivemq.com'

    # create the client with a fixed ID
    client_name = 'Randomizer'
    client = mqtt.Client(client_name)
    client.on_connect = cll.log_on_connect
    # client.on_message = cll.log_on_message
    # no need for declaring the `on_message` callback since the publisher does not subscribe to the topic
    client.on_publish = cll.log_on_publish

    # the topic name that is used within the current method
    local_topic = topic.Topic()

    # establish connection to the broker
    print(f'Establishing connection...')
    client.connect(Hive_Broker)

    # start the loop (handshake with the server so the CONNACK completes)
    # add a pause before pulling and publishing the message, so the CONNACK will finish
    client.loop_start()
    print(f'Loop started...')
    cll.wait(1)

    startime = 0
    while startime < run_time:
        # generate a message
        print(f'Preparing the message to be published...')
        full_msg = mess.pullMessage()
        print(f'Will publish -> {full_msg["message"]} | {full_msg["size"]}')
        # publish the message after it was generated
        client.publish(local_topic, str(full_msg["message"]))
        startime = startime + 1
        cll.wait(1)

    # keep the loop running until the runtime as finished
    # cll.wait(run_time)
    # stop the loop after the message has been published
    client.loop_stop()
    print(f'Loop ended')
