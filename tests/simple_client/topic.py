def Topic():
    with open('topic', 'r+') as reader:
        topic = str(reader.readline().strip())
    try:
        assert len(topic) > 0
    except AssertionError as err:
        print('Error while reading the topic file')
        return -1
    else:
        return topic


if __name__ == '__main__':
    topic = Topic()
    if topic != -1:
        print(topic)
