
# helper functions
topic_file_name = lambda topic: f'{topic}_data_mqtt.dat'
utf8_decode = lambda msg: str(msg.decode('utf-8'))


def ClearFile(topic):
    with open(topic_file_name(topic), 'w') as cleaner:
        cleaner.truncate(0)


def SaveToFile(topic, msg):
    with open(topic_file_name(topic), 'a+') as writer:
        writer.write(msg)
        writer.write('\n')


class MQTT_Publish_Callbacks:
    def log_on_connect(client, userdata, flags, rc):
        print(f'Connection established with result code -> {rc} 🤝')

    def log_on_disconnect(client, userdata, rc):
        if rc != 0:
            print('Unexpected disconnection.')
        else:
            print(f'Disconnected from the broker with result code -> {rc} ❌')

    def log_on_publish(client, userdata, mid):
        print(f'Published to the server with mid: {mid}')


class MQTT_Subscribe_Callbacks:
    def log_on_connect(client, userdata, flags, rc):
        print(f'Connection established with result code -> {rc} 🤝')

    def log_on_disconnect(client, userdata, rc):
        if rc != 0:
            print('Unexpected disconnection.')
        else:
            print(f'Disconnected from the broker with result code -> {rc} ❌')

    def log_on_subscribe(client, userdata, mid, granted_qos):
        print(f'Subscribed to the server with mid: {mid}')

    def log_on_unsubscribe(client, userdata, mid):
        print(f'Unsubscribed from the server with mid: {mid}')

    def log_on_message(client, userdata, message):
        # get the topic name from the message
        topic = str(message.topic)
        # retrieve the [raw] sent message in binary form
        encoded_message = message.payload
        # convert in a string
        decoded_message = utf8_decode(encoded_message)
        SaveToFile(topic, decoded_message)
        print(
            f'MSG: {decoded_message} + QoS: {str(message.qos)} on Topic: {topic}')
