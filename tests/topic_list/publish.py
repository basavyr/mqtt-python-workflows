from paho.mqtt import client as mqtt
import time
import get_topics as topics
import callbacks as calls

import paho.mqtt.publish as publish

from random import uniform, randrange


def wait(wait_time):
    """
    Block the current thread for a duration of `wait_time` seconds.
    """
    time.sleep(wait_time)


def messager():
    ret_array = [randrange(0, 10, 1) for _ in range(10)]
    return ret_array


class MQTT_Publisher:
    # ****** CHANGE TO ACTUAL LIST OF TOPICS ******
    test_topic = ['bishu1', 'bishu2', 'bishu3']
    # *********************************************

    def __init__(self, topicList):
        self.topicList = topicList
        self.broker = 'broker.hivemq.com'
        self.clientName = 'RandomArrays'

    def PublishMessages(self, messager, loop_time):
        # create the client using the mqtt constructor
        client = mqtt.Client(self.clientName)

        client.on_connect = calls.MQTT_Publish_Callbacks.log_on_connect
        client.on_disconnect = calls.MQTT_Publish_Callbacks.log_on_disconnect
        client.on_publish = calls.MQTT_Publish_Callbacks.log_on_publish

        start_time = 0

        # establish connection between the client and the pre-defined broker
        client.connect(host=self.broker)

        # create a loop between the server/broker and the publisher client
        client.loop_start()
        wait(1)

        DMs = [(MQTT_Publisher.test_topic[0], 'bishu-content1'), (MQTT_Publisher.test_topic[1],
                                                                  'bishu-content2'), (MQTT_Publisher.test_topic[2], 'bishu-content3')]

        while start_time <= loop_time:
            msg = str(messager())
            # print(f'Publishing MESSAGE #{start_time} -> {msg}')
            publish.multiple(msgs=DMs, hostname=self.broker)
            start_time = start_time + 1
            wait(1)

        client.loop_stop()
        client.disconnect()


def main():
    publisher = MQTT_Publisher(topics.Topics())
    publisher.PublishMessages(messager=messager, loop_time=100)


if __name__ == '__main__':
    main()
