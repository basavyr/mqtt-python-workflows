from paho.mqtt import client as mqtt
import time
import get_topics as topics
import callbacks as calls


import paho.mqtt.subscribe as subscribe


def wait(wait_time):
    time.sleep(wait_time)


class MQTT_Subscriber:
    # ****** CHANGE TO ACTUAL LIST OF TOPICS ******
    test_topic = ['bishu1', 'bishu2', 'bishu3']
    # *********************************************

    def __init__(self, topicList):
        self.topicList = topicList
        self.broker = 'broker.hivemq.com'
        self.clientName = 'ArraySubscriber'

    def GetMessages(self, loop_time):
        # create the client using the mqtt constructor
        client = mqtt.Client(self.clientName)

        # settings the callbacks
        client.on_connect = calls.MQTT_Subscribe_Callbacks.log_on_connect
        client.on_subscribe = calls.MQTT_Subscribe_Callbacks.log_on_subscribe
        client.on_unsubscribe = calls.MQTT_Subscribe_Callbacks.log_on_unsubscribe
        client.on_message = calls.MQTT_Subscribe_Callbacks.log_on_message

        start_time = 0

        # establish connection between the client and the pre-defined broker
        client.connect(host=self.broker)

        # create a loop between the server/broker and the publisher client
        client.loop_start()
        wait(1)

        client.subscribe(MQTT_Subscriber.test_topic)

        # clear every topic-specific file from old data
        calls.ClearFile(MQTT_Subscriber.test_topic)

        while start_time <= loop_time:
            start_time = start_time + 1
            wait(1)

        client.unsubscribe(MQTT_Subscriber.test_topic)
        wait(1)

        client.loop_stop()
        client.disconnect()

    def PullMessages(self):
        # create the client that subscribes to the list of topics
        client = mqtt.Client(client_id=self.clientName)

        # settings the callbacks
        client.on_connect = calls.MQTT_Subscribe_Callbacks.log_on_connect
        client.on_subscribe = calls.MQTT_Subscribe_Callbacks.log_on_subscribe
        client.on_unsubscribe = calls.MQTT_Subscribe_Callbacks.log_on_unsubscribe
        client.on_message = calls.MQTT_Subscribe_Callbacks.log_on_message

        # establish connection to the server
        client.connect(host=self.broker)

        # subscribe to the topic
        # client.subscribe(MQTT_Subscriber.test_topic[0])
        # subscribe.simple(topics=MQTT_Subscriber.test_topic[0],
        #                  hostname=self.broker)
        subscribe.callback(callback=calls.MQTT_Subscribe_Callbacks.log_on_message,
                           topics=MQTT_Subscriber.test_topic, hostname=self.broker)

        # start the loop
        client.loop_forever()


def main():
    subscriber = MQTT_Subscriber(topics.Topics())
    subscriber.PullMessages()


if __name__ == '__main__':
    main()
