
def Topics():
    # extract the topics from the file
    topic_list = []
    with open('.topics', 'r+') as reader:
        lines = reader.readlines()
    for line in lines:
        topic_list.append(str(line.strip()))
    # make sure the topic list is not empty
    try:
        assert len(topic_list) > 0
    except AssertionError as err:
        print('There are no topics within the list')
        return -1
    else:
        pass
    return topic_list
