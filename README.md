# `MQTT`-Python Workflows
Collection of different implementations that the `MQTT` standard for machine-to-machine communication. Applications are developed in Python.


## Resources

1. MQTT brokers that are available for free:
   1. https://test.mosquitto.org/
   2. https://mqtt.eclipseprojects.io/
2. Guides for understanding the mqtt protocol:
   1. [MQTT Beginners Guide](https://medium.com/python-point/mqtt-basics-with-python-examples-7c758e605d4)
   2. [mqtt-python](https://github.com/code-and-dogs/mqtt-python)
   3. [yt-tutorial -> MQTT Beginner Guide with Python](https://www.youtube.com/watch?v=kuyCd53AOtg)